export ZSH="/home/clangford/.oh-my-zsh"
export PATH=$PATH:/usr/local/bin:/usr/local/sbin:~/Applications:/usr/local/go/bin:/home/clangford/go/bin
alias nvim='nvim.appimage'
alias obsidian='obsidian.appimage'
alias awslocal='aws --profile local --endpoint-url "http://localhost:4566"'
alias w='nitrogen --set-zoom-fill --random ~/Documents/src/wallpapers/wallpapers'
alias startes='cd ~/Documents/src/edsights && ./startes.sh'
alias stopes='cd ~/Documents/src/edsights && ./stop.sh'
alias src='cd ~/Documents/src'
export VISUAL=nvim.appimage
export EDITOR=$VISUAL
export JS_LIBRARY_READ_PACKAGE_REGISTRY_TOKEN="3quGXbbyMy4_czzRJ218"

ZSH_THEME="robbyrussell"
bindkey -v
source $ZSH/oh-my-zsh.sh

# fnm
export PATH=/home/clangford/.fnm:$PATH
eval "`fnm env`"

export PATH="$PATH:/usr/bin/elixir"

export PGHOST="edsights-staging-encrypted-1.ckn4dsad8y1c.us-east-1.rds.amazonaws.com"
export PGPASSWORD="$(aws rds generate-db-auth-token --hostname $PGHOST --port 5432 --region us-east-1 --username chuck )"
export PGSSLMODE="verify-full"
export PGSSLROOTCERT="/home/clangford/Desktop/us-east-1-bundle.pem"

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/terraform terraform

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/clangford/google-cloud-sdk/path.zsh.inc' ]; then . '/home/clangford/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/clangford/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/clangford/google-cloud-sdk/completion.zsh.inc'; fi
