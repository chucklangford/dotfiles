local lspconfig_util = require("lspconfig.util")

local custom_attach = function()
    vim.keymap.set("n", "K", vim.lsp.buf.hover, {buffer=0})
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, {buffer=0})
    vim.keymap.set("n", "<leader>j", vim.diagnostic.goto_next, {buffer=0})
    vim.keymap.set("n", "<leader>k", vim.diagnostic.goto_prev, {buffer=0})
end

-- install: https://quick-lint-js.com/install/cli/debian/
require('lspconfig').quick_lint_js.setup{
    on_attach = custom_attach
}

-- install: sudo npm i -g vscode-langservers-extracted
require('lspconfig').eslint.setup{
    on_attach = custom_attach
}

-- install: sudo npm install -g @volar/vue-language-server
require('lspconfig').volar.setup{
    init_options = {
        typescript = {
            serverPath = '/usr/lib/node_modules/typescript/lib/tsserverlibrary.js'
        }
    },
    on_attach = custom_attach
}

require('lspconfig').pyright.setup{
    on_attach = custom_attach
}
