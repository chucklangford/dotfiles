local cs = {}

cs.Cheatsheet = function()
    local width = 80
    local height = 20
    local buf = vim.api.nvim_create_buf(false, true)
    vim.keymap.set('n', '<esc>', "<cmd>:q<cr>", { silent = true, buffer = buf })
    local hey = vim.api.nvim_buf_set_lines(buf, 0, -1, true, { "test", "text" })
    vim.api.nvim_buf_set_option(buf, "modifiable", false)
    local ui = vim.api.nvim_list_uis()[1]
    local opts = {relative='editor', width=width, height=height, col=(ui.width/2)-(width/2), row=(ui.height/2)-(height/2), anchor='NW', style='minimal', border='single'}
    local win = vim.api.nvim_open_win(buf, 0, opts)
    local there = vim.api.nvim_win_set_option(win, 'winhl', 'Normal:MyHighlight')
end

return cs
