require('telescope').load_extension('fzf')
require("toggleterm").setup{
	open_mapping = [[<c-j>]],
	shade_terminals = true,
	direction = 'float'
}
require('leap').add_default_mappings()

vim.g.mapleader = " "
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.colorcolumn = "80"
vim.opt.termguicolors = true
vim.o.background = "dark"
vim.opt.completeopt={"menu", "menuone", "noselect"}
vim.cmd([[colorscheme gruvbox]])
vim.cmd([[cd ~/Documents/src]])
vim.cmd([[set clipboard+=unnamedplus]])
vim.cmd([[filetype plugin indent on]])
vim.api.nvim_exec([[ autocmd FileType javascript setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4 ]], false)
vim.api.nvim_exec([[ autocmd FileType go setlocal noexpandtab shiftwidth=4 tabstop=4 softtabstop=4 ]], false)
vim.api.nvim_exec([[ autocmd FileType html setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4 ]], false)
vim.api.nvim_exec([[ autocmd FileType css setlocal noexpandtab shiftwidth=4 tabstop=4 softtabstop=4 ]], false)
vim.api.nvim_exec([[ autocmd FileType scss setlocal noexpandtab shiftwidth=4 tabstop=4 softtabstop=4 ]], false)
vim.api.nvim_exec([[ highlight BadWhitespace ctermbg=Red guibg=Red ]], false)
vim.api.nvim_exec([[ au BufRead,BufNewFile *.js,*.py,*.pyw match BadWhitespace /^\t\+/ ]], false)
vim.api.nvim_exec([[ au BufRead,BufNewFile *.* match BadWhitespace /\s\+$/ ]], false)
