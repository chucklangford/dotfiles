-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
  use 'wbthomason/packer.nvim'
  use 'ellisonleao/gruvbox.nvim'
  use 'tpope/vim-surround'
  use 'tpope/vim-commentary'
  use 'tpope/vim-fugitive'
  use 'nvim-lua/plenary.nvim'
  use 'fatih/vim-go'
  use 'SirVer/ultisnips'
  use 'honza/vim-snippets'
  use 'neovim/nvim-lspconfig'
  use 'nvim-telescope/telescope.nvim'
  use 'nvim-treesitter/nvim-treesitter'
  use { 'junegunn/fzf', run = function() vim.fn['fzf#install']() end }
  use 'junegunn/fzf.vim'
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'quangnguyen30192/cmp-nvim-ultisnips'
  use {"akinsho/toggleterm.nvim", tag = 'v2.0.0', config = function()
	  require("toggleterm").setup()
  end}
  use 'ggandor/leap.nvim'
  use 'tpope/vim-repeat'
end)
