call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-vinegar'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'neovim/nvim-lspconfig'
Plug 'folke/lsp-colors.nvim'
call plug#end()

let mapleader=" " "Remap the leader key
set number "Show line numbers
set relativenumber "Use relative line numbers
set hidden "Hide buffer when you switch to another buffer
set hlsearch "Highlight all the found search patterns
set foldmethod=syntax "Use language syntax when folding lines
set list "show whitespace
set completeopt=menuone,noinsert,noselect "Set completeopt to have a better completion experience
set shortmess+=c "Avoid showing message extra message when using completion
set colorcolumn=80 "Colors the specified column
set clipboard+=unnamedplus
filetype plugin indent on "Turn on known filetype syntax and indentation
colorscheme gruvbox "Set the colors
cd ~/Documents/src "Set the default working directory

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"   "Navigate forward through popup menu
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>" "Navigate backward through popup menu
inoremap jk <esc>
nmap <silent> <leader>ev :e $MYVIMRC<CR> "Edit the vimrc
nnoremap <leader>bb :Buffers<CR>
nnoremap <leader>ff :Files<CR>

highlight BadWhitespace ctermbg=Red guibg=Red "Create a highlight group
au BufRead,BufNewFile *.js,*.py,*.pyw match BadWhitespace /^\t\+/
au BufRead,BufNewFile *.* match BadWhitespace /\s\+$/

augroup config_group
  autocmd FileType javascript setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4
  autocmd FileType html setlocal noexpandtab shiftwidth=4 tabstop=4 softtabstop=4
  autocmd FileType css setlocal noexpandtab shiftwidth=4 tabstop=4 softtabstop=4
  autocmd FileType scss setlocal noexpandtab shiftwidth=4 tabstop=4 softtabstop=4
augroup END

lua << EOF
local nvim_lsp = require('lspconfig')
require("lsp-colors").setup({
  Error = "#db4b4b",
  Warning = "#e0af68",
  Information = "#0db9d7",
  Hint = "#10B981"
})

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
end

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { "pyls", "tsserver", "vuels" }
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup { on_attach = on_attach }
end

EOF
